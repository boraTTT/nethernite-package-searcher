import { createStore } from 'vuex';
import api from '../api/axiosInstance';

const store = createStore({
  state: {
    library: null,
  },

  actions: {
    getLibraryByName: (context, name) => {
      api.get(name).then(res => {
        console.log('res: ', res)
      })
    }
  }
});

export default store;