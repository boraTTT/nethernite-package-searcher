import './assets/main.css';
import 'vuetify/_styles.scss';
import '@mdi/font/css/materialdesignicons.css'

import { createApp } from 'vue';
import { createVuetify } from 'vuetify';
import { components, directives } from 'vuetify/dist/vuetify';
import store from './store';
import App from './App.vue';

const vuetify = createVuetify({
  icons: {
    iconfont: 'mdi',
  },
  components,
  directives,
});

createApp(App)
  .use(store)
  .use(vuetify)
  .mount('#app');
